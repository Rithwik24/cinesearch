<h1>CineSearch</h1>

<h3>Overview</h3>
This project is a website developed using Next.js, enabling users to explore 20 popular movies categorized by genres. The application integrates with the TMDB API to fetch real-time movie data, showcasing expertise in web development, API integration, and user-centric design. The project leverages the following technologies and libraries:

<ul type="circle">
<li>Next.js: Utilized for building efficient and scalable web applications.</li>
<li>TMDB API: Integrated to fetch real-time movie data.</li>
<li>CSS (or Styled Components): Styled the components for a clean and user-friendly interface.</li>
<li>React: Used for building the user interface and managing the application's state.</li>
</ul>

<h3>Features</h3>
<ul type="circle">
<li>Dynamic Content: Display 20 popular movies based on different genres.</li>
<li>Light and Dark Mode: Allow users to switch between light and dark themes for a personalized experience.</li>
<li>TMDB API Integration: Utilize the TMDB API for real-time movie information.</li>
</ul>

<h3>Installation</h3>
To run the project locally, follow these steps:
<ol type="1">
<li>Clone the repository to your local machine:

`git clone https://gitlab.com/Rithwik24/cinesearch.git`</li>

<li>Navigate to the project directory: 

`cd cinesearch`</li>

<li>Install dependencies:
<ul type="circle">
<li>npm install</li>
<li>npm install react</li>
<li>npm install next</li>
<li>npm install react-redux</li>
</ul>

<li>Get your TMDB API key:

<ul type="circle">
<li>Visit https://developer.themoviedb.org/ to create an account and request an API key.</li>
<li>In next.config.js file, set the TMDB_API_KEY environment variable to your API key</li>
</ul>

`TMDB_API_KEY:your-api-key-here`</li>

<li>Start the development server:

`npm run dev`</li>

<li>Open your web browser and access the website at http://localhost:3000.</li>
</ol>

<h3>License</h3>
This project is licensed under the MIT License. You can find the full license information in the LICENSE file.