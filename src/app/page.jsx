import React, {useContext} from 'react';
import Image from 'next/image';
import home from "../../public/home.png"; 
import styles from "./page.module.css";
import Link from 'next/link';

export default function Home() {

  return (
    <div className={styles.container}>
      <div className={styles.item}>
        <h1 className={styles.title}>Explore Cinematic Diversity: Discover 20 Popular Movies, Sorted by Genres, at CineSearch!</h1>
        <p className={styles.desc}>CineSearch is your gateway to cinematic diversity! Uncover a curated selection of 20 popular movies, thoughtfully categorized by genres, right here on our platform.</p>
        <Link className={styles.button} href="/genres">CineSearch</Link>
      </div>
      <div className={styles.item}>
        <Image className={styles.img} src={home}/>
      </div>
    </div>
  )
}