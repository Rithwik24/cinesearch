import React from 'react';
// import Image from 'next/image';
// import about from '../../../public/about.jpg';
import styles from './page.module.css'; 
import Link from 'next/link';

async function getMovie() {
  const res = await fetch('https://api.themoviedb.org/3/genre/movie/list?api_key=d108c77634ef90559ceb57441c37e4aa',{
    cache: "no-store"
  })
  if(!res.ok){
    throw new Error("Failed to fetch");
  }
  return res.json();
}

export const metadata = {
  title: 'Movie Geners',
  description: 'This is movie geners',
}

const Genres = async () => {
  const movieGenre = await getMovie();
  // const movie = JSON.stringify(movieCategory);
  // console.log(movie)
  // console.log(movieCategory.genres[0].name);
  return (
    <div className={styles.container}>
      {
        movieGenre.genres.map((genre) => 
        <Link href={`/genres/${genre.id}`} key={genre.id} className={styles.list}>
          <h3>{genre.name}</h3>
        </Link>
        )
      }
    </div>
  )
}
export default Genres