import React from 'react';
import styles from './page.module.css'
import Image from 'next/image';
import Link from 'next/link';

async function getGenre(id, num) {
    const res = await fetch(`https://api.themoviedb.org/3/discover/movie?api_key=${process.env.TMDB_API_KEY}&with_genres=${id}&page=${num}`,{
      cache: "no-store"
    })
    if(!res.ok){
      throw new Error("Failed to fetch");
    }
    return res.json();
}

export function generateMetaData({params}){

    return {
        title: params.id,
        description: params.id,
    };
}

const CategoryId = async ({params}) => {
    const movies = await getGenre(params.id, 1);

    return (
        <>
        <Link href="../genres" style={{width:"10px"}}>Back</Link>
        
            <div className={styles.container}>
                {
                    movies.results.map((movie) => 
                    <>
                    <Link href={`/genres/${params.id}/${movie.id}`}>
                        <div className={styles.poster} key={movie.id}>
                            <h3>{movie.original_title}</h3>
                            <h5>Language:{movie.original_language}</h5>
                            <div className={styles.imgs}>{movie.poster_path !== null ? <Image loading="lazy" src={`https://image.tmdb.org/t/p/original${movie.poster_path}`} width={200} height={238} style={{ boxShadow: "5px 5px 2em black" }} alt="movie image" /> : <div style={{ width: "200px", height: "238px", backgroundColor: "white" }} />}</div>
                        </div>
                    </Link>
                    </>
                    )
                }
            </div>
        </>
    )
}

export default CategoryId