import React from 'react';
import styles from './page.module.css';
import Link from 'next/link';
import Image from 'next/image';

async function getMovie(id) {
    const res = await fetch(`https://api.themoviedb.org/3/movie/${id}?api_key=${process.env.TMDB_API_KEY}`,{
      cache: "no-store"
    })
    if(!res.ok){
      throw new Error("Failed to fetch");
    }
    return res.json();
}

const CategoryPage = async ({params}) => {
    const movie = await getMovie(params.movie);
    return (
        <>
                <Link href={`../${params.id}`} style={{width:"10px"}}>Back</Link>
                <div className={styles.container}>
                    <div className={styles.imgs}>{movie.poster_path !== null ? <Image loading="lazy" src={`https://image.tmdb.org/t/p/original${movie.poster_path}`} width={378} height={549} alt="movie image" /> : <div style={{ width: "378px", height: "549px", backgroundColor: "white" }} />}</div>
                    <div className={styles.desc}>
                        <h1>{movie.original_title}</h1>
                        <h3>Language:{movie.original_language}</h3>
                        <h3>Runtime: {movie.runtime} minutes</h3>
                        <h3>Movie Overview:</h3>
                        <p>{movie.overview}</p>
                    </div>
                </div>
        </>
    )
}

export default CategoryPage