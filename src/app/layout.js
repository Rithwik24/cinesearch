import Navbar from '@/components/navbar/Navbar';
import './globals.css';
import { Inter, Lato } from 'next/font/google';
import Footer from '@/components/footer/Footer';
import { ThemeProvider } from '../context/themeprovider';

const inter = Inter({ subsets: ['latin'] })
const lato = Lato({subsets: ['latin'], weight: ['900']})

export const metadata = {
  title: 'CineSearch',
  description: 'This is CineSearch',
}

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <ThemeProvider>
            <div>
              <div className='container'>
                <Navbar/>
                {children}
                <Footer/>
              </div>
            </div>
        </ThemeProvider>
      </body>
    </html>
  )
}
