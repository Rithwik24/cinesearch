import React from 'react';
import styles from './page.module.css';
import contact from '../../../public/contact.png';
import Image from 'next/image'; 

export const metadata = {
  title: 'Contact',
  description: 'This is contact',
}

const Contact = () => {
  return (
    <div className={styles.container}>
      <h1 className={styles.title}>Need movie recommendations or have feedback? Reach out!</h1>
      <div className={styles.content}> 
        <div>
            <form className={styles.form}>
              <input className={styles.text} style={{marginTop: "30px"}}type="text" placeholder="Name" />
              <input className={styles.text} type="text" placeholder="Email" />
              <textarea className={styles.textarea} placeholder="Message" cols={30} rows={10}></textarea>
              <button className={styles.button}>Send</button>
            </form>
          </div>
          <div className={styles.img}>
            <Image className={styles.img} src={contact}/>
          </div>
      </div>
    </div>
  )
}

export default Contact