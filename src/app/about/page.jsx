import React from 'react';
import styles from "./page.module.css";
import Image from 'next/image';
import about from "../../../public/about.jpg";
import about1 from '../../../public/about1.jpg';
import about2 from '../../../public/about2.jpg';

export const metadata = {
  title: 'About',
  description: 'This is about',
}

export const About = () => {
  return (
    <div className={styles.container}>
      <div className={styles.image}> 
        <Image alt="header-img" src={about}/>
      </div>
      <div className={styles.desc}>
          <h1>Welcome to CineSearch</h1>
          Our mission is simple:  to curate an exceptional selection of the 20 most popular movies for your cinematic enjoyment.
      </div>
      <div className={styles.content}>
          <Image alt='about-img1' className={styles.img1} src={about1} />
        <div className={styles.contentdesc}>
          <h1>Discover 20 Popular Movies: 🍿</h1>
          With an ever-expanding library of films, we&apos;ve handpicked 20 of the most popular movies for your viewing pleasure. These aren&apos;t just any films; they&apos;re the ones that have captivated audiences and critics alike, creating lasting memories and sparking passionate discussions.
        </div>
      </div>
      <div className={styles.content}>
        <div className={styles.contentdesc}>
          <h2>Sorted by Genres: 🎬</h2>
          Finding the perfect movie shouldn&apos;t be a daunting task. That&apos;s why we&apos;ve organized our selection by genres. Whether you&apos;re in the mood for a spine-tingling thriller or a heartwarming family drama, our intuitive genre-based sorting makes your search effortless.
        </div>
        <Image alt='about-img2' className={styles.img1} src={about2} />
      </div>
      Explore, discover, and enjoy the magic of cinema with us. Your next movie adventure awaits! 🍿🎬🌟
    </div>
  )
}

export default About