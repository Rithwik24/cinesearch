"use client"

import React, {useContext} from 'react';
import styles from './toggledarkmode.module.css';
import {ThemeContext} from '@/context/themeprovider';

const ToggleDarkMode = () => {
  const { toggle,mode } = useContext(ThemeContext);
  const modeClassName = mode === 'light' ? styles.togglelight : styles.toggledark;

  return (
    <div className={`${styles.toggle} ${modeClassName}`} onClick={toggle}>
    <button className={styles.button} style={mode === 'light' ? {color:'black'} : {color:'white'}}>{mode === "light" ? "Light Mode" : "Dark Mode"}</button>
    </div>
  )
}

export default ToggleDarkMode