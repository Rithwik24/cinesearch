import React from 'react'
import styles from './footer.module.css'
import Image from 'next/image'

const Footer = () => {
  return (
    <div className={styles.footer}>
        <div>ⓒ2023 CineSearch. All rights reserved</div>
        <div className={styles.imgSocial}>
          <Image className={styles.icon} src="/fb.png" width={15} height={15} alt='CineBot fb'/>
          <Image className={styles.icon} src="/ig.png" width={15} height={15} alt='CineBot ig'/>
          <Image className={styles.icon} src="/tw.png" width={15} height={15} alt='CineBot tw'/>
          <Image className={styles.icon} src="/yt.png" width={15} height={15} alt='CineBot yt'/>
        </div>
    </div>
  )
}

export default Footer