"use client"

import Link from 'next/link';
import React from 'react';
import styles from './navbar.module.css';
import ToggleDarkMode from '../toggledarkmode/ToggleDarkMode';

const links = [
  {
    id: 1,
    title: "Home",
    url: "/"
  },
  {
    id: 2,
    title: "Movie Genres",
    url: "/genres"
  },
  {
    id: 3,
    title: "About",
    url: "/about"
  },
  {
    id: 4,
    title: "Contact",
    url: "/contact"
  }
]

const Navbar = (props) => {

  return (
    <div className={styles.navbar}>
        <Link className={styles.logo} href="/">CineSearch</Link>
        <div className={styles.links}>
          {links.map((link) => (<Link key={link.id} href={link.url}>{link.title}</Link>))}
        </div>
        <ToggleDarkMode/>
    </div>
  )
}

export default Navbar